import 'bootstrap/dist/css/bootstrap.min.css';
import Routers from './routers';
import { Container } from 'react-bootstrap'

function App() {
  return (
    <Container>
      <Routers />
    </Container>
  );
}

export default App;
