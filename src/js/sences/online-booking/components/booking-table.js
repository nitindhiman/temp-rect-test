import {Table, Button} from 'react-bootstrap';

function BookingTable(props) {
    console.log("BookingTable :-",props)
    return(
        <Table responsive>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Availability</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {
                    props.data.map( (item,index) => {
                        return(
                            <tr key={index}>
                                <td>{item.date}</td>
                                <td>{item.time}</td>
                                <td>{`${item.availableSeats} seat avaialabe`}</td>
                                <td>
                                    {
                                        item.availableSeats == 0 ?
                                        <Button className="book-full" disabled>Full</Button>
                                        :
                                        props.buttonType === "add" ?
                                            <Button className="book-now" onClick={ ()=>{props.handleClick(index,item)}}>Book Now</Button>
                                            :
                                            <Button className="book-now" onClick={ ()=>{props.handleClick(index)}}>Cancel</Button>
                                    }
                                </td>
                            </tr>
                        )
                    })
                }
            </tbody>
        </Table>
    )
}

export default BookingTable;