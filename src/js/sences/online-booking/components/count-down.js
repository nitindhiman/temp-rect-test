import { useEffect, useState } from 'react';

export default function CountDown(props) {
    const [countDown, setCountDown] = useState( Math.random() > .5 ? 60 : 30);
    useEffect(() => {
        countDown > 0 ? setTimeout(() => setCountDown(countDown - 1), 1000) : props.setFreeSeats(0)
    }, [countDown]);
    return(
        <h3 className="coutdown">Time Left: {countDown} seconds</h3>
    )
}