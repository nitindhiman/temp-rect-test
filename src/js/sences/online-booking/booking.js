import './booking.css'
import { Badge, Row, Col } from 'react-bootstrap';
import CountDown from './components/count-down';
import { useEffect, useState } from 'react';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';
import _ from 'lodash';
import BookingTable from './components/booking-table';

function Booking(props) {
    const classess = ["physics","math","chemistry","arts","programing","sports","drwaing","yoga","dance","vocational","gym","crossfit","hairdressing","makeup","Designing"];
    const [ bookingFullCount , setBookingFull ] = useState(0);
    const [ data, setData ] = useState([]);
    const [ badgeCount, setBadgeCount ] = useState(0);
    const [ freeSeats, setFreeSeats] = useState(Math.floor(Math.random() * 11) + 5);

    //Work as componentDidMount
    useEffect( () => {
        setData(classess.map( (item,i) => {
            const hours = Math.floor(Math.random() * 13 ) + 1;
            console.log("hors ",hours)
            return (
                {
                    _id : uuidv4(),
                    date : moment().add(i, 'days').format("ddd MMM DD YYYY"),
                    time: hours <= 8 ? `${moment().hours(hours).format("hh:00")} PM - ${moment().hours(hours+1).format("hh:00")} PM` :  `${moment().hours(hours).format("hh:00 A")} - ${moment().hours(hours+1).format("hh:00 A")}`,
                    availableSeats : setAvaiableSeats()
                }
            )
        }))
        let parse = JSON.parse(localStorage.getItem("cart"));
        if(typeof parse === 'object' && Array.isArray(parse?.data) && parse?.data?.length>0){
            setBadgeCount(parse.data.length);
        }
    },[]);

    function setAvaiableSeats(){
        const random = Math.floor(Math.random() * 10) + 1 ;
        if(bookingFullCount<5){
            setBookingFull(prevCount => prevCount + 1);
            let flag = Math.floor(Math.random() * 2)
            let returnvalue = random * flag;
            return returnvalue
        }else{
            return random 
        }
    }

    const handleClick = (index,item) => {
        if(item.availableSeats>0){
            let parse = JSON.parse(localStorage.getItem("cart"));
            if(typeof parse === 'object' && Array.isArray(parse?.data) && parse?.data?.length>0){
                _.findIndex(parse.data, { '_id': data._id}) 
                parse.data.push(item);
                localStorage.setItem("cart", JSON.stringify(parse))
                setBadgeCount(parse.data.length)
            }else{
                localStorage.setItem("cart", JSON.stringify({ "data" : [item] }) )
            }
            let cloneData = [...data];
            cloneData[index].availableSeats = item.availableSeats - 1;
            setData(cloneData);
        }
    }

    return(
        <>
            <div className="bg-image">
                <Row>
                    <Col>
                        <CountDown setFreeSeats={setFreeSeats}/>
                        <h2 className="free-trail">Claim Your Free Trail Class</h2>
                    </Col>
                    <Col>
                        <div class="cart-badge">
                            <Link to="/classes/cart">
                                <ShoppingCartOutlinedIcon fontSize="large"/> <Badge variant="light">{badgeCount}</Badge>
                            </Link>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h2 className="class-schedule">Class Schedule</h2>
                    </Col>
                    <Col className="col-sm-6">
                        <h2 className="free-seats">Free Seats Left: <span className="free-trail">{freeSeats}</span></h2>
                    </Col>
                </Row>
            </div>
            <BookingTable data={data} handleClick={handleClick} buttonType="add"/>
        </>
    )
}

export default Booking;