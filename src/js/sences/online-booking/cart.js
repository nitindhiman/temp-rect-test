import './booking.css';
import { useEffect, useState } from 'react';
import CartTable from './components/booking-table';
import _ from 'lodash';

function Cart(props) {
    const [ data, setData ] = useState([]);

    //Work as componentDidMount
    useEffect( ()=> {
        let parse = JSON.parse(localStorage.getItem("cart"));
        console.log(parse)
        if(typeof parse === 'object' && Array.isArray(parse?.data) && parse?.data?.length>0){
            setData(parse.data)
        }
    },[]);

    const handleDelete = (index) => {
        let cloneData = [ ...data];
        cloneData.splice(index,1);
        localStorage.setItem("cart", JSON.stringify({ data: cloneData}) );
        setData(cloneData);
    }

    return(
        <CartTable data={data} handleClick={handleDelete} buttonType="delete"/>
    )
}

export default Cart;