import {BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Booking from "./sences/online-booking/booking";
import Cart from "./sences/online-booking/cart";

function Routers(){
    return(
        <Router basename="/">
            <Switch>
                <Route exact path="/classes/booking" render={(props) =><Booking { ...props}/>}/>
                <Route exact path="/classes/cart" render={(props) =><Cart { ...props}/>}/>
            </Switch>
        </Router>
    )
}

export default Routers;